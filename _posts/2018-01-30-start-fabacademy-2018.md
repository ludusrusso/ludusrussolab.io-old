---
title: Fab Academy First Posts
layout: post
date: 2018-01-30
tag:
 - fabacademy
category: blog
author: ludusrusso
---

Hey,

this is my first post on the **Ludus Russo Jekyll Template** developed for the
[Fab Academy 2018](http://fabacademy.org/).

This is my better image:

![Fab Academy Image](/assets/images/fabac.jpg)
